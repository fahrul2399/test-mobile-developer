import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:quote_app/model/quote/quote.dart';
import 'package:http/http.dart' as http;

class QuoteRemoteDatasource {
  Future<Either<String, Quote>> getQuote() async {
    final headers = {'Content-Type': 'application/json'};
    final Uri url = Uri.parse("https://api.quotable.io/random");
    final response = await http.get(url, headers: headers);
    if (response.statusCode == 200) {
      return Right(Quote.fromJson(jsonDecode(response.body)));
    } else {
      return const Left("Internal Server Errro");
    }




    
  }
}
