import 'package:flutter/material.dart';

class CardQuote extends StatefulWidget {
  const CardQuote({
    super.key,
    required this.quote,
    required this.author,
    required this.action,
  });

  final String quote;
  final String author;
  final Widget action;

  @override
  State<CardQuote> createState() => _CardQuoteState();
}

class _CardQuoteState extends State<CardQuote> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey[300]!, width: 2),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(16),
            child: Text(
              widget.quote,
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w500,
                color: Colors.grey[600],
              ),
            ),
          ),
          Center(
            child: Text(
              '~ ${widget.author} ~',
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Colors.grey[600],
              ),
            ),
          ),
          Padding(padding: const EdgeInsets.all(16), child: widget.action)
        ],
      ),
    );
  }
}
