import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  const MyButton({
    super.key,
    required this.title,
    required this.type,
    required this.onPressed,
  });

  final String title;
  final String type;
  final VoidCallback onPressed;

  Color getColor() {
    if (type == "primary") {
      return const Color(0xff0d6efd);
    } else if (type == "secondary") {
      return const Color(0xff198754);
    } else if (type == "danger") {
      return const Color(0xffdc3545);
    } else {
      return const Color(0xff0d6efd);
    }
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        height: 50,
        decoration: BoxDecoration(
          color: getColor(),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Center(
          child: Text(
            title,
            style: const TextStyle(
              fontSize: 16,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}
