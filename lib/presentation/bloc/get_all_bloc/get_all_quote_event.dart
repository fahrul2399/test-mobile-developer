part of 'get_all_quote_bloc.dart';

@freezed
class GetAllQuoteEvent with _$GetAllQuoteEvent {
  const factory GetAllQuoteEvent.getQuote() = _GetQuote;
}
