import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:quote_app/dataSource/quote_remote_datasource.dart';
import 'package:quote_app/model/quote/quote.dart';

part 'get_all_quote_event.dart';
part 'get_all_quote_state.dart';
part 'get_all_quote_bloc.freezed.dart';

class GetAllQuoteBloc extends Bloc<GetAllQuoteEvent, GetAllQuoteState> {
  GetAllQuoteBloc() : super(const _Initial()) {
    on<_GetQuote>(
      (event, emit) async {
        emit(const _Loading());
        final response = await QuoteRemoteDatasource().getQuote();
        response.fold(
          (l) => emit(_Error(l)),
          (r) => emit(
            _Loaded(r),
          ),
        );
      },
    );
  }
}
