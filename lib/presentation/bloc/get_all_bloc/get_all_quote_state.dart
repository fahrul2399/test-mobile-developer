part of 'get_all_quote_bloc.dart';

@freezed
class GetAllQuoteState with _$GetAllQuoteState {
  const factory GetAllQuoteState.initial() = _Initial;
  const factory GetAllQuoteState.loading() = _Loading;
  const factory GetAllQuoteState.loaded(Quote quote) = _Loaded;
  const factory GetAllQuoteState.error(String message) = _Error;
}
