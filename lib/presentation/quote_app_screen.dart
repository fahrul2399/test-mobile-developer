// ignore_for_file: camel_case_types, prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:quote_app/widgets/mybutton.dart';

import '../model/quote/quote.dart';
import '../widgets/card_quote.dart';
import 'bloc/get_all_bloc/get_all_quote_bloc.dart';

class QuoteAppScreen extends StatefulWidget {
  const QuoteAppScreen({Key? key}) : super(key: key);

  @override
  State<QuoteAppScreen> createState() => _QuoteAppScreenState();
}

class _QuoteAppScreenState extends State<QuoteAppScreen> {
  List<Quote> favoriteQuote = [];

  void getQuote() {
    context.read<GetAllQuoteBloc>().add(const GetAllQuoteEvent.getQuote());
  }

  void addFavoriteQuote(Quote quote) {
    if (favoriteQuote.contains(quote)) return;
    setState(() {
      favoriteQuote.add(quote);
    });
  }

  void deleteFavoriteQuote(Quote quote) {
    setState(() {
      favoriteQuote.remove(quote);
    });
  }

  @override
  void initState() {
    getQuote();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            "Quote App",
            style: TextStyle(color: Colors.white),
          ),
          actions: const [],
          backgroundColor: Colors.deepPurple,
        ),
        body: Padding(
          padding: const EdgeInsets.all(16),
          child: ListView(
            children: [
              BlocConsumer<GetAllQuoteBloc, GetAllQuoteState>(
                listener: (context, state) {},
                builder: (context, state) {
                  return state.maybeWhen(
                    loading: () => const Center(
                      child: CircularProgressIndicator(),
                    ),
                    loaded: (quote) {
                      return CardQuote(
                        quote: '" ${quote.content} " ',
                        author: quote.author ?? "",
                        action: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            MyButton(
                              title: "New Quote",
                              type: "primary",
                              onPressed: () {
                                getQuote();
                              },
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            MyButton(
                              title: "Add Favorite",
                              type: "secondary",
                              onPressed: () {
                                addFavoriteQuote(quote);
                              },
                            ),
                          ],
                        ),
                      );
                    },
                    orElse: () => const SizedBox(),
                  );
                },
              ),
              const SizedBox(
                height: 20,
              ),
              const Center(
                child: Text(
                  "Favorite Quote",
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              if (favoriteQuote.isNotEmpty)
                Column(
                  children: favoriteQuote
                      .map(
                        (item) => Padding(
                          padding: const EdgeInsets.only(bottom: 20),
                          child: CardQuote(
                            quote: '" ${item.content} " ',
                            author: item.author ?? "",
                            action: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                MyButton(
                                  title: "Delete",
                                  type: "danger",
                                  onPressed: () {
                                    deleteFavoriteQuote(item);
                                  },
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                              ],
                            ),
                          ),
                        ),
                      )
                      .toList(),
                )
            ],
          ),
        ));
  }
}
